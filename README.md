# Welcome!

# RandomContact APP

Welcome to this sample project. You will find an very simple application with two screens:

- [MainActivity](https://gitlab.com/Esquivel/contactdetailexample/-/blob/main/app/src/main/java/com/facu/contactdetailexample/main/MainActivity.kt) / [MainFragment](https://gitlab.com/Esquivel/contactdetailexample/-/blob/main/app/src/main/java/com/facu/contactdetailexample/main/ui/MainFragment.kt):
    Here you will find an screen with a extended floating action button (FAB), contact list, filter tabs and a sort menu.
    Each contact card support 3 different actions:
     * Add to favourites.
     * Remove contact from list and send it to a banned contact list.
     * Go to the detail contact screen.

     
* 1- To add contacts you can use the extended FAB, this will consume a api, obtain 10 contacts, parse and validate that information, and show in the list. Sometimes you will see only a few values because some of that raw data is corrupted or cannot pass the validations. All of this validations are encapsulated on [ContactMapper](https://gitlab.com/Esquivel/contactdetailexample/-/blob/main/app/src/main/java/com/facu/contactdetailexample/main/interactor/ContactMapper.kt) -> [unit test](https://gitlab.com/Esquivel/contactdetailexample/-/blob/main/app/src/test/java/com/facu/contactdetailexample/main/interactor/ContactMapperTest.kt)
* 2 - This contacts will be sort by last name by default but you can change this sort dinamically using the sort menu.
* 3 - Also you can filter contacts, hide some contacts that is not representative for you. At this moment you can filter by near you (at this moment has a dummy logic that will compare each contact location to Madrid and filter those values far from there - 2KM), or you can filter by your favourites (the favourites are stored in memory, any logic related to database or sharedPreference is included in this example).

- [DetailActivity](https://gitlab.com/Esquivel/contactdetailexample/-/blob/main/app/src/main/java/com/facu/contactdetailexample/detail/DetailActivity.kt):
    This screen will show the detailed information of a Contact clicked on MainActivity.
    In this example you will see how to implement:
    * Communication between Activities using Intent
    * Communication between Fragment and their Activity.
    * A collapsible toolbar with an image and animation.


Important desing points:
- All objects are inmutable, mutability can able undesired behaviors and should be avoided on concurrent scenarios. 
- Domain objects are defined of interfaces, concrete classes are encapsulated inside creational objects.
- All objects are desinged to use dependency injection, this is specially important because throught this design we will can create unit tests.
- You will find restrictions related to instantiation behaviors, you only can create a instances using or a Builder, or a Factory or Provider.
- To desing objects with one responsibiltity and also offer flexibility a Chain of responsibility class was created [BehaviorMediator](https://gitlab.com/Esquivel/contactdetailexample/-/blob/main/app/src/main/java/com/facu/contactdetailexample/main/interactor/behavior/BehaviorMediator.kt). Is called Mediator because it defines how a set of objects will interact between each other, but on a deep level you will see the core logic is process a Request by multiple Receivers. Internally each Receiver has not reference to the next Receiver, instead this is delegated into Mediator.


## UX

This app is inspired by the [Material Design Guidelines](https://material.io/design).
It was designed supporting [accessibility](https://developer.android.com/guide/topics/ui/accessibility) best practices  

## Architecture
Here will find examples to how implement:

- [Model View ViewModel](https://developer.android.com/jetpack/guide#recommended-app-arch) This is a special implementation developed by Google included on [JetPack Libraries](https://developer.android.com/jetpack)

## Languages
This app was developed 98% on Kotlin.

## Unit test
You will find examples of how test two extremely different layers: [Mapper](https://gitlab.com/Esquivel/contactdetailexample/-/blob/main/app/src/test/java/com/facu/contactdetailexample/main/interactor/ContactMapperTest.kt), [Interactor](https://gitlab.com/Esquivel/contactdetailexample/-/blob/main/app/src/test/java/com/facu/contactdetailexample/main/interactor/MainDataInteractorTest.kt) and [Behavior](https://gitlab.com/Esquivel/contactdetailexample/-/blob/main/app/src/test/java/com/facu/contactdetailexample/main/interactor/behavior/remove/RemoveContactBehaviorTest.kt)

