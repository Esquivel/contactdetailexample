package com.facu.contactdetailexample.main.interactor.behavior.remove

import com.facu.contactdetailexample.main.interactor.behavior.ContactActionRepository
import com.facu.contactdetailexample.main.interactor.behavior.InternalCallback
import com.facu.contactdetailexample.main.interactor.model.MainData
import com.facu.contactdetailexample.main.interactor.model.MainDataBuilder
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*

/**
 * Here you will find an example of a concrete type of [Behavior<T: BehaviorTask>].
 *
 * All validations on this layer are critical because will affect to the behavior of one or more
 * behavior operations.
 *
 * Only a few test is written, to show an example of what should be checked. In a real project
 * we will has one test to each concrete type of [Behavior<T: BehaviorTask>].
 */
class RemoveContactBehaviorTest {

    private lateinit var behavior: RemoveContactBehavior
    private lateinit var mainDataBuilder: MainDataBuilder
    private lateinit var repository: ContactActionRepository
    private lateinit var mainData: MainData
    private lateinit var behaviorTask: RemoveBehaviorTask

    @Before
    fun setup() {
        mainDataBuilder = mock(MainDataBuilder::class.java)
        repository = mock(ContactActionRepository::class.java)
        mainData = mock(MainData::class.java)
        behaviorTask = mock(RemoveBehaviorTask::class.java)

        `when`(mainDataBuilder.withContactList(anyList())).thenReturn(mainDataBuilder)
        `when`(mainDataBuilder.withFilterList(anyList())).thenReturn(mainDataBuilder)
        `when`(mainDataBuilder.withSort(any())).thenReturn(mainDataBuilder)
        `when`(mainDataBuilder.withMainData(mainData)).thenReturn(mainDataBuilder)
        `when`(mainDataBuilder.build()).thenReturn(mainData)

        behavior = RemoveContactBehavior(mainDataBuilder, repository)
    }

    @Test
    fun `WHEN finish processing THEN must call to onFinishProcess`() = runBlocking {
        val callback = mock(InternalCallback::class.java)

        behavior.process(mainData, behaviorTask, callback)

        verify(callback).onFinishProcess(mainDataBuilder.build())
    }

    @Test
    fun `WHEN process THEN must call to repository save with task inputData`() = runBlocking {
        val callback = mock(InternalCallback::class.java)

        behavior.process(mainData, behaviorTask, callback)

        verify(repository).save(behaviorTask.inputData)
    }
}