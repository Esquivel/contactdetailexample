package com.facu.contactdetailexample.main.interactor

import com.facu.contactdetailexample.main.interactor.repository.ContactRepository
import com.facu.contactdetailexample.main.interactor.repository.model.*
import org.junit.Assert
import org.junit.Before
import org.junit.Test

private const val EMPTY = ""
private const val STRING = "string"

/**
 * Here you will find examples of unit tests on [ContactMapper].
 *
 * This class is responsible to filter those invalid values of [ContactDTO], an success filter
 * able an easier work with domain data minimizing validations and centralizing on the source,
 * which is [ContactRepository] in this case.
 *
 * Only a few test is written, to show an example of what should be checked. In a real project
 * we will has one test to each attribute.
 */
class ContactMapperTest {

    private lateinit var contactMapper: ContactMapper

    @Before
    fun setup() {
        contactMapper = ContactMapper()
    }

    @Test
    fun `WHEN has a contactDto with idDto with null THEN must be filtered`() {
        val contactDTO = ContactDTO(null, null, null,
                null, null, null, null)
        val list = listOf(contactDTO)

        Assert.assertEquals(0, contactMapper.map(list).size)
    }

    @Test
    fun `WHEN nameDto first is empty THEN must be filtered`() {
        val list = listOf(createDummy(NameDto(STRING, EMPTY, STRING)))

        Assert.assertEquals(0, contactMapper.map(list).size)
    }

    @Test
    fun `WHEN nameDto last is empty THEN must be filtered`() {
        val list = listOf(createDummy(NameDto(STRING, STRING, EMPTY)))

        Assert.assertEquals(0, contactMapper.map(list).size)
    }

    @Test
    fun `WHEN has a valid contactDto THEN must be added to list`() {
        val list = listOf(createDummy())

        Assert.assertEquals(1, contactMapper.map(list).size)
    }

    @Test
    fun `WHEN has a duplicated valid contactDto THEN only one must be added to list`() {
        val list = listOf(createDummy(), createDummy(), createDummy(), createDummy())

        Assert.assertEquals(1, contactMapper.map(list).size)
    }

    private fun createDummy(nameDto: NameDto? = null): ContactDTO {
        val name = nameDto ?: createNameDtoDummy()
        val idDto = IdDto(STRING, STRING)
        val locationDto = LocationDto(CoordinatesDto(STRING, STRING))
        val pictureDto = PictureDto(STRING, STRING)
        return ContactDTO(STRING, idDto, locationDto, STRING, STRING, name, pictureDto)
    }

    private fun createNameDtoDummy(): NameDto {
        return NameDto(STRING, STRING, STRING)
    }
}