package com.facu.contactdetailexample.main.interactor

import com.facu.contactdetailexample.main.interactor.behavior.Behavior
import com.facu.contactdetailexample.main.interactor.behavior.BehaviorMediator
import com.facu.contactdetailexample.main.interactor.behavior.BehaviorProvider
import com.facu.contactdetailexample.main.interactor.behavior.aggregation.AggregationBehaviorTask
import com.facu.contactdetailexample.main.interactor.behavior.favourite.FavBehaviorTask
import com.facu.contactdetailexample.main.interactor.behavior.filter.FilterBehaviorTask
import com.facu.contactdetailexample.main.interactor.behavior.remove.RemoveBehaviorTask
import com.facu.contactdetailexample.main.interactor.behavior.sort.SortBehaviorTask
import com.facu.contactdetailexample.main.interactor.behavior.sort.SortProvider
import com.facu.contactdetailexample.main.interactor.model.MainData
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*

/**
 * Here you will find examples of unit tests on [MainDataInteractor].
 *
 * In this example we make some test related to object interactions, on action
 * [MainDataInteractor.onAddContactsCalled]. To be completed this should have coverage on each
 * action but this is just an example or unit testing.
 *
 * This is a critical logic and should have coverage to ensure a correct behavior.
 */
class MainDataInteractorTest {

    private lateinit var mainInteractor: MainDataInteractor
    private lateinit var behaviorMediator: BehaviorMediator
    private lateinit var mainData: MainData
    private lateinit var behaviorProvider: BehaviorProvider

    private lateinit var aggregationBehavior: Behavior<AggregationBehaviorTask>
    private lateinit var sortBehavior: Behavior<SortBehaviorTask>
    private lateinit var filterBehavior: Behavior<FilterBehaviorTask>
    private lateinit var favBehavior: Behavior<FavBehaviorTask>
    private lateinit var removeBehavior: Behavior<RemoveBehaviorTask>
    private lateinit var taskBehaviorFactory: TaskBehaviorFactory


    @Before
    fun setup() {
        behaviorMediator = mock(BehaviorMediator::class.java)
        mainData = mock(MainData::class.java)
        behaviorProvider = mock(BehaviorProvider::class.java)
        aggregationBehavior = mock(Behavior::class.java) as Behavior<AggregationBehaviorTask>
        sortBehavior = mock(Behavior::class.java) as Behavior<SortBehaviorTask>
        filterBehavior = mock(Behavior::class.java) as Behavior<FilterBehaviorTask>
        favBehavior = mock(Behavior::class.java) as Behavior<FavBehaviorTask>
        removeBehavior = mock(Behavior::class.java) as Behavior<RemoveBehaviorTask>
        taskBehaviorFactory = mock(TaskBehaviorFactory::class.java)

        val sortProvider = mock(SortProvider::class.java)

        `when`(behaviorProvider.provideAggregationBehavior()).thenReturn(aggregationBehavior)
        `when`(behaviorProvider.provideSortBehavior()).thenReturn(sortBehavior)
        `when`(behaviorProvider.provideFilterBehavior()).thenReturn(filterBehavior)
        `when`(behaviorProvider.provideFavBehavior()).thenReturn(favBehavior)
        `when`(behaviorProvider.provideRemoveBehavior()).thenReturn(removeBehavior)

        mainInteractor = MainDataInteractor(
            behaviorMediator,
            behaviorProvider,
            mainData,
            sortProvider,
            taskBehaviorFactory
        )
    }

    @Test
    fun `WHEN called to stop THEN mediator must stop`() {
        mainInteractor.stop()

        verify(behaviorMediator).stop()
    }

    @Test
    fun `WHEN onAddContactsCalled THEN must call to makeAggregation`(): Unit = runBlocking {
        mainInteractor.onAddContactsCalled()

        verify(taskBehaviorFactory, times(1)).makeAggregation()
    }

    @Test
    fun `WHEN onAddContactsCalled THEN must call to makeSort`(): Unit = runBlocking {
        mainInteractor.onAddContactsCalled()

        verify(taskBehaviorFactory, times(1)).makeSort(mainData.currentSort)
    }

    @Test
    fun `WHEN onAddContactsCalled THEN must call to makeFav`(): Unit = runBlocking {
        mainInteractor.onAddContactsCalled()

        verify(taskBehaviorFactory, times(1)).makeFav()
    }

    @Test
    fun `WHEN onAddContactsCalled THEN must call to makeFilter`(): Unit = runBlocking {
        mainInteractor.onAddContactsCalled()

        verify(taskBehaviorFactory, times(1)).makeFilter()
    }
}