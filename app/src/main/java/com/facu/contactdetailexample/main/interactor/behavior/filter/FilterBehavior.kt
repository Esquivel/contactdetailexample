package com.facu.contactdetailexample.main.interactor.behavior.filter

import android.location.Location
import com.facu.contactdetailexample.commons.location.LocationFacade
import com.facu.contactdetailexample.commons.location.LocationFacadeCallback
import com.facu.contactdetailexample.main.Filter
import com.facu.contactdetailexample.main.interactor.behavior.Behavior
import com.facu.contactdetailexample.main.interactor.behavior.BehaviorType
import com.facu.contactdetailexample.main.interactor.behavior.InternalCallback
import com.facu.contactdetailexample.main.interactor.model.Contact
import com.facu.contactdetailexample.main.interactor.model.MainData
import com.facu.contactdetailexample.main.interactor.model.MainDataBuilder

private const val TWO_KILOMETER = 1.609344 * 2

class FilterBehavior(
        private val mainDataBuilder: MainDataBuilder,
        private val locationFacade: LocationFacade,
        private val filterProvider: FilterProvider
): Behavior<FilterBehaviorTask> {

    private var currentLocation: Location? = null
    private val favFilteredList = ArrayList<Contact>()
    private val nearMeFilteredList = ArrayList<Contact>()

    init {
        locationFacade.setCallback(object : LocationFacadeCallback {
            override fun onInvalidOperation(message: String) {
                currentLocation = null
            }

            override fun onNotifyEvent(message: String) {
                //Nothing to do.
            }

            override fun onLocationUpdated(location: Location) {
                currentLocation = location
                locationFacade.stopUpdates()
            }

        })
    }

    override fun getType(): BehaviorType = BehaviorType.FILTER

    override suspend fun process(
            previousData: MainData,
            specialData: FilterBehaviorTask,
            callback: InternalCallback
    ) {
        val internalData: MutableList<Contact> = ArrayList(previousData.contactList)

        val updatedFilterList: List<Filter> = filterProvider
                .provideFilterList(specialData.filter, previousData.filterList)

        for (value in updatedFilterList) {
            if (value.type == FilterType.NEAR_ME) {
                nearMeFilter(value, internalData)
                continue
            }

            if (value.type == FilterType.ONLY_FAV) {
                onlyFavFilter(value, internalData)
            }
        }

        callback.onFinishProcess(mainDataBuilder
                .withMainData(previousData)
                .withContactList(internalData)
                .withFilterList(updatedFilterList)
                .build())
    }

    private fun onlyFavFilter(filter: Filter, contacts: MutableList<Contact>) {
        if (!filter.enabled) {
            contacts.addAll(favFilteredList)
            favFilteredList.clear()
            return
        }

        for (contact in contacts) {
            if (!contact.favourite) {
                favFilteredList.add(contact)
            }
        }

        contacts.removeAll(favFilteredList)
    }

    private fun nearMeFilter(filter: Filter, contacts: MutableList<Contact>) {
        if (!filter.enabled) {
            contacts.addAll(nearMeFilteredList)
            nearMeFilteredList.clear()
            return
        }

        if (currentLocation == null) {
            locationFacade.startUpdates(0)
        }

        val targetLocation = Location("")
        for (contact in contacts) {
            targetLocation.latitude = contact.coordinates.lat.toDouble()
            targetLocation.longitude = contact.coordinates.lon.toDouble()

            val distance: Float = targetLocation.distanceTo(currentLocation)

            if (distance > TWO_KILOMETER) {
                nearMeFilteredList.add(contact)
            }
        }

        contacts.removeAll(nearMeFilteredList)
    }
}