package com.facu.contactdetailexample.main.interactor.behavior

import com.facu.contactdetailexample.main.interactor.model.MainData

class BehaviorMediator {

    private val behaviorMap = HashMap<BehaviorType, Behavior<BehaviorTask>>()

    private var callback: ContactBehaviorCallback? = null
    private var shouldStop = false

    /**
     * You can add the supported behaviors using this method.
     */
    fun <T: BehaviorTask> addBehavior(behavior: Behavior<T>) {
        this.behaviorMap[behavior.getType()] = behavior as Behavior<BehaviorTask>
    }

    /**
     * The set callback method.
     */
    fun setCallback(callback: ContactBehaviorCallback) {
        this.callback = callback
    }

    /**
     * Start process with a data [data] on a specified order [taskOrder].
     * This process is asynchronous and recursive.
     *
     * Returns the result on [ContactBehaviorCallback.onFinish]
     */
    suspend fun <T: BehaviorTask> start(data: MainData, taskOrder: List<T>) {
        if (shouldStop) {
            return
        }

        if (taskOrder.isEmpty()) {
            callback?.onFinish(data)
            shouldStop = false
            return
        }

        val internalTasks = ArrayList(taskOrder)
        val nextTask: BehaviorTask = internalTasks[0]
        internalTasks.remove(nextTask)

        behaviorMap[nextTask.type]?.run {
            this.process(data, nextTask, object : InternalCallback {
                override suspend fun onFinishProcess(result: MainData) {
                    start(result, internalTasks)
                }
            })
        }
    }

    /**
     * Stops the processing.
     */
    fun stop() {
        shouldStop = true
    }
}