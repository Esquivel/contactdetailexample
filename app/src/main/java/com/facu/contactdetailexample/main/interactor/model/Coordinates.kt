package com.facu.contactdetailexample.main.interactor.model

import java.io.Serializable

interface Coordinates: Serializable {

    /**
     * Returns the latitude
     */
    val lat: String

    /**
     * Returns the longitude
     */
    val lon: String
}