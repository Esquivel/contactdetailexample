package com.facu.contactdetailexample.main.interactor.repository.model

import com.facu.contactdetailexample.main.interactor.model.Picture
import com.google.gson.annotations.SerializedName

data class PictureDto(
    @SerializedName("large")
    override val largeUri: String,
    @SerializedName("thumbnail")
    override val miniUri: String
): Picture