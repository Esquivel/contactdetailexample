package com.facu.contactdetailexample.main.interactor.behavior

import com.facu.contactdetailexample.main.interactor.model.MainData

interface InternalCallback {

    suspend fun onFinishProcess(result: MainData)
}