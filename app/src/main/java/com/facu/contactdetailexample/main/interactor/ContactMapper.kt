package com.facu.contactdetailexample.main.interactor

import androidx.annotation.WorkerThread
import com.facu.contactdetailexample.main.interactor.model.*
import com.facu.contactdetailexample.main.interactor.repository.model.ContactDTO

@WorkerThread
class ContactMapper {

    /**
     * Map from ContactDto to MainData.
     * Here is performed all validations.
     */
    fun map(dtoContacts: List<ContactDTO>): List<Contact> {
        val distinctContacts = removeDuplicates(dtoContacts)

        return filterInvalids(distinctContacts)
    }

    /**
     * Remove those values that are duplicated.
     */
    private fun removeDuplicates(dtoContacts: List<ContactDTO>): Set<ContactDTO> = HashSet<ContactDTO>(dtoContacts)

    /**
     * Apply some validations before add this contact to the target collection.
     */
    private fun filterInvalids(contacts: Set<ContactDTO>): List<Contact> {
        val filteredSet = ArrayList<Contact>()
        
        for (contact in contacts) {
            if (contact.id.isEmpty()) {
                continue
            }

            if (contact.name.first.isEmpty()
                || contact.name.last.isEmpty()) {
                continue
            }

            if (contact.coordinates.lat.isEmpty()
                || contact.coordinates.lon.isEmpty()) {
                continue
            }

            if (contact.email.isEmpty()) {
                continue
            }

            if (contact.phone.isEmpty()) {
                continue
            }

            if (contact.picture.largeUri.isEmpty()
                || contact.picture.miniUri.isEmpty()) {
                continue
            }

            filteredSet.add(contact)
        }

        return filteredSet
    }
}