package com.facu.contactdetailexample.main.interactor.behavior

import com.facu.contactdetailexample.main.interactor.model.Contact

/**
 * This class encapsulates an access to a source of data. This could be a database or
 * sharedPreferences.
 *
 * In this example I will use an internal state approach, sharing this same instance between
 * it clients.
 */
class InMemoryRepository: ContactActionRepository {

    private val ignoredContacts = ArrayList<Contact>()

    override fun load(): List<Contact> {
        return ignoredContacts
    }

    override fun save(contactList: List<Contact>) {
        ignoredContacts.addAll(contactList)
    }
}