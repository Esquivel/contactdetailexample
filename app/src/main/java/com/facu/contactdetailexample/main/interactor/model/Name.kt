package com.facu.contactdetailexample.main.interactor.model

import java.io.Serializable

interface Name: Serializable {

    /**
     * Returns the first name.
     */
    val first: String

    /**
     * Returns the last name.
     */
    val last: String
}