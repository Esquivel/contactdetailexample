package com.facu.contactdetailexample.main.interactor.repository.model

import com.facu.contactdetailexample.main.interactor.model.Name

data class NameDto(
    val title: String,
    override val first: String,
    override val last: String
): Name