package com.facu.contactdetailexample.main

import com.facu.contactdetailexample.main.interactor.model.Contact

interface MainCallback {

    fun onClickDetail(contact: Contact)
}