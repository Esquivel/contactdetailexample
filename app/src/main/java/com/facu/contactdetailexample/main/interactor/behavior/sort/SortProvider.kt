package com.facu.contactdetailexample.main.interactor.behavior.sort

import com.facu.contactdetailexample.main.SortType

class SortProvider {

    fun provideDefaultSort(): Sort {
        return InternalSort(SortType.LAST_NAME)
    }

    fun provideSortBy(sortType: SortType): Sort {
        return when (sortType) {
            SortType.FIRST_NAME -> InternalSort(SortType.FIRST_NAME)
            SortType.LAST_NAME -> InternalSort(SortType.LAST_NAME)
            SortType.COORDINATES -> InternalSort(SortType.COORDINATES)
            SortType.GENDER -> InternalSort(SortType.GENDER)
            SortType.PHONE -> InternalSort(SortType.PHONE)
            SortType.MAIL -> InternalSort(SortType.MAIL)
            else -> InternalSort(SortType.FIRST_NAME)
        }
    }

    class InternalSort(private val sortType: SortType): Sort {

        override val stringRes: Int
            get() = sortType.stringRes

        override val type: SortType
            get() = sortType

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as InternalSort

            if (sortType != other.sortType) return false

            return true
        }

        override fun hashCode(): Int {
            return sortType.hashCode()
        }
    }
}