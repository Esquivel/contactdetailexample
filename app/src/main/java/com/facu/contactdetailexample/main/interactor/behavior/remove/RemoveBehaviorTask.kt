package com.facu.contactdetailexample.main.interactor.behavior.remove

import com.facu.contactdetailexample.main.interactor.behavior.BehaviorType
import com.facu.contactdetailexample.main.interactor.behavior.BehaviorTask
import com.facu.contactdetailexample.main.interactor.model.Contact

class RemoveBehaviorTask(val inputData: List<Contact>): BehaviorTask {
    override val type: BehaviorType
        get() = BehaviorType.REMOVE
}