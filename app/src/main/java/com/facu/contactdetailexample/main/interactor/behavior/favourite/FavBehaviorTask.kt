package com.facu.contactdetailexample.main.interactor.behavior.favourite

import com.facu.contactdetailexample.main.interactor.behavior.BehaviorType
import com.facu.contactdetailexample.main.interactor.behavior.BehaviorTask
import com.facu.contactdetailexample.main.interactor.model.Contact

class FavBehaviorTask(val contact: Contact? = null) : BehaviorTask {

    override val type: BehaviorType = BehaviorType.FAV
}