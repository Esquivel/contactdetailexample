package com.facu.contactdetailexample.main.interactor.behavior

import com.facu.contactdetailexample.commons.ContactApiBuilder
import com.facu.contactdetailexample.commons.location.DummyLocationWorker
import com.facu.contactdetailexample.main.interactor.ContactMapper
import com.facu.contactdetailexample.main.interactor.behavior.aggregation.AggregationBehaviorTask
import com.facu.contactdetailexample.main.interactor.behavior.aggregation.AggregationContactBehavior
import com.facu.contactdetailexample.main.interactor.behavior.favourite.FavBehaviorTask
import com.facu.contactdetailexample.main.interactor.behavior.remove.RemoveBehaviorTask
import com.facu.contactdetailexample.main.interactor.behavior.favourite.FavContactBehavior
import com.facu.contactdetailexample.main.interactor.behavior.sort.SortBehaviorTask
import com.facu.contactdetailexample.main.interactor.behavior.favourite.InternalFavouriteRepository
import com.facu.contactdetailexample.main.interactor.behavior.filter.FilterBehavior
import com.facu.contactdetailexample.main.interactor.behavior.filter.FilterBehaviorTask
import com.facu.contactdetailexample.main.interactor.behavior.filter.FilterProvider
import com.facu.contactdetailexample.main.interactor.behavior.remove.RemoveContactBehavior
import com.facu.contactdetailexample.main.interactor.behavior.sort.SortBehavior
import com.facu.contactdetailexample.main.interactor.model.MainDataBuilder
import com.facu.contactdetailexample.main.interactor.repository.ContactApiClient
import com.facu.contactdetailexample.main.interactor.repository.ContactRepository
import com.facu.contactdetailexample.main.interactor.behavior.sort.SortProvider

class BehaviorProvider {

    private val ignoredContactRepository = InMemoryRepository()
    private val favContactRepository = InternalFavouriteRepository()

    fun provideAggregationBehavior(): Behavior<AggregationBehaviorTask> {
        return AggregationContactBehavior(
                ContactRepository(provideContactApi()),
                ContactMapper(),
                provideMainDataBuilder(),
                ignoredContactRepository
        )
    }

    fun provideRemoveBehavior(): Behavior<RemoveBehaviorTask> {
        return RemoveContactBehavior(provideMainDataBuilder(), ignoredContactRepository)
    }

    fun provideFavBehavior(): Behavior<FavBehaviorTask> {
        return FavContactBehavior(favContactRepository, provideMainDataBuilder())
    }

    fun provideSortBehavior(): Behavior<SortBehaviorTask> {
        return SortBehavior(provideSortProvider(), provideMainDataBuilder())
    }

    fun provideFilterBehavior(): Behavior<FilterBehaviorTask> {
        return FilterBehavior(provideMainDataBuilder(), DummyLocationWorker(), FilterProvider())
    }

    private fun provideMainDataBuilder() = MainDataBuilder()

    private fun provideSortProvider(): SortProvider = SortProvider()

    private fun provideContactApi(): ContactApiClient {
        return ContactApiBuilder().withLoggingInterceptor().build()
    }
}