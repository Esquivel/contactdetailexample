package com.facu.contactdetailexample.main.interactor.behavior.filter

import com.facu.contactdetailexample.main.Filter
import com.facu.contactdetailexample.main.interactor.behavior.BehaviorTask
import com.facu.contactdetailexample.main.interactor.behavior.BehaviorType

class FilterBehaviorTask(val filter: Filter? = null): BehaviorTask {

    override val type: BehaviorType = BehaviorType.FILTER
}