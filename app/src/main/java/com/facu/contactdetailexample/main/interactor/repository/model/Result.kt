package com.facu.contactdetailexample.main.interactor.repository.model

import com.google.gson.annotations.SerializedName

data class Result(
    @SerializedName("results")
    val contactList: List<ContactDTO>
)