package com.facu.contactdetailexample.main.interactor.behavior

import com.facu.contactdetailexample.main.interactor.behavior.BehaviorType

interface BehaviorTask {

    val type: BehaviorType
}