package com.facu.contactdetailexample.main.interactor.behavior.favourite

import com.facu.contactdetailexample.main.interactor.model.Contact
import com.facu.contactdetailexample.main.interactor.model.Coordinates
import com.facu.contactdetailexample.main.interactor.model.Name
import com.facu.contactdetailexample.main.interactor.model.Picture

open class FavouriteContact(
        private val contact: Contact,
        private val isFavourite: Boolean
): Contact {

    override val id: String
        get() = contact.id

    override val name: Name
        get() = contact.name

    override val gender: String
        get() = contact.gender

    override val picture: Picture
        get() = contact.picture

    override val email: String
        get() = contact.email

    override val phone: String
        get() = contact.phone

    override val coordinates: Coordinates
        get() = contact.coordinates

    override val favourite: Boolean
        get() = isFavourite

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as FavouriteContact

        if (contact != other.contact) return false

        return true
    }

    override fun hashCode(): Int {
        return contact.hashCode()
    }
}