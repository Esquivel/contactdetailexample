package com.facu.contactdetailexample.main.interactor.model

import com.facu.contactdetailexample.main.Filter
import com.facu.contactdetailexample.main.interactor.behavior.sort.Sort

class MainDataBuilder {

    private val contactList: MutableList<Contact> = ArrayList()
    private val filterList: MutableList<Filter> = ArrayList()
    private var sort: Sort? = null

    fun withMainData(mainData: MainData): MainDataBuilder {
        withContactList(mainData.contactList)
        withFilterList(mainData.filterList)
        withSort(mainData.currentSort)
        return this
    }

    fun withContactList(contactList: List<Contact>): MainDataBuilder {
        this.contactList.clear()
        this.contactList.addAll(contactList)
        return this
    }

    fun withFilterList(filterList: List<Filter>): MainDataBuilder {
        this.filterList.clear()
        this.filterList.addAll(filterList)
        return this
    }

    fun withSort(sort: Sort?): MainDataBuilder {
        this.sort = sort
        return this
    }

    fun build(): MainData {
        return Internal(contactList, filterList, sort)
    }

    private class Internal(
            override val contactList: List<Contact>,
            override val filterList: List<Filter>,
            override val currentSort: Sort?
    ) : MainData {
        override val currentFilter: Filter?
            get() {
                for (filter in filterList) {
                    if (filter.enabled) {
                        return filter
                    }
                }

                return null
            }
    }
}