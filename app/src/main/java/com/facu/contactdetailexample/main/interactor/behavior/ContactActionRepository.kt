package com.facu.contactdetailexample.main.interactor.behavior

import com.facu.contactdetailexample.main.interactor.model.Contact

/**
 * This repository is an abstraction of each action that user can apply to a contact and that
 * action is important so it must be saved and restored.
 *
 * Example of this action:
 *  - IGNORE (delete users)
 */
interface ContactActionRepository {

    /**
     * Load contacts.
     */
    fun load(): List<Contact>

    /**
     * Save contacts.
     */
    fun save(contactList: List<Contact>)
}