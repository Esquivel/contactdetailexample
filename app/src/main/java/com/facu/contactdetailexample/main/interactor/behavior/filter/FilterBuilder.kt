package com.facu.contactdetailexample.main.interactor.behavior.filter

import com.facu.contactdetailexample.main.Filter

class FilterProvider {

    fun provideFilterList(current: Filter?, filters: List<Filter>): List<Filter> {
        val collection = HashSet<Filter>()

        current?.run {
            collection.add(InternalFilter(this.type, !this.enabled))
        }

        collection.addAll(filters)

        collection.add(InternalFilter(FilterType.NEAR_ME))
        collection.add(InternalFilter(FilterType.ONLY_FAV))

        val list = ArrayList(collection)
        return list.sortedByDescending { it.enabled }
    }

    class InternalFilter(
            private val filterType: FilterType,
            override val enabled: Boolean = false
    ) : Filter {

        override val stringRes: Int
            get() = filterType.stringRes

        override val type: FilterType
            get() = filterType

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as InternalFilter

            if (type != other.type) return false

            return true
        }

        override fun hashCode(): Int {
            return javaClass.hashCode()
        }
    }
}