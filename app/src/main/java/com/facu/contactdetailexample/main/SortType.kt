package com.facu.contactdetailexample.main

import com.facu.contactdetailexample.R

enum class SortType(val value: Int, val stringRes: Int) {

    FIRST_NAME(1, R.string.first_name_type_description),
    LAST_NAME(2, R.string.last_name_type_description),
    COORDINATES(3, R.string.coordinates_type_description),
    GENDER(4, R.string.gender_type_description),
    MAIL(5, R.string.mail_type_description),
    PHONE(6, R.string.phone_type_description);
}