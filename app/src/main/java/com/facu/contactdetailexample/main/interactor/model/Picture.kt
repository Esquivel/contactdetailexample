package com.facu.contactdetailexample.main.interactor.model

import java.io.Serializable

interface Picture: Serializable {

    /**
     * Returns the large image Uri.
     */
    val largeUri: String

    /**
     * Returns the mini image Uri.
     */
    val miniUri: String
}