package com.facu.contactdetailexample.main.interactor.behavior

enum class BehaviorType {

    AGGREGATION,
    REMOVE,
    FAV,
    SORT,
    FILTER
}