package com.facu.contactdetailexample.main.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.facu.contactdetailexample.R;
import com.facu.contactdetailexample.main.Filter;

import java.util.ArrayList;
import java.util.List;

public class FilterAdapter extends ListAdapter<Filter, FilterAdapter.ItemViewHolder> {

    @Nullable
    private Callback callback;

    public FilterAdapter() {
        super(DIFF_CALLBACK);
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_filter, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        final Filter filter = getItem(position);

        final Context context = holder.filterTextView.getContext();

        holder.filterTextView.setText(context.getString(filter.getStringRes()));
        holder.container.setBackgroundColor(ContextCompat.getColor(context, filter.getEnabled()
                ? android.R.color.holo_green_dark
                : android.R.color.holo_blue_light));
        holder.container.setOnClickListener(view -> {
            if (callback != null) {
                callback.onFilterClicked(filter);
            }
        });
    }

    @Override
    public void submitList(@Nullable List<Filter> list) {
        super.submitList(list != null ? new ArrayList<>(list) : null);
    }

    public void setCallback(@Nullable final Callback callback) {
        this.callback = callback;
    }

    static final class ItemViewHolder extends RecyclerView.ViewHolder {

        final View container;
        final TextView filterTextView;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);

            filterTextView = itemView.findViewById(R.id.textSort);
            container = itemView.findViewById(R.id.container);
        }
    }

    public interface Callback {

        void onFilterClicked(@NonNull final Filter filter);
    }

    static final DiffUtil.ItemCallback<Filter> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<Filter>() {
                @Override
                public boolean areItemsTheSame(@NonNull Filter oldItem, @NonNull Filter newItem) {
                    return oldItem.getType().equals(newItem.getType())
                            && oldItem.getEnabled() == newItem.getEnabled();
                }

                @Override
                public boolean areContentsTheSame(@NonNull Filter oldItem, @NonNull Filter newItem) {
                    return oldItem.getStringRes() == newItem.getStringRes()
                            && oldItem.getEnabled() == newItem.getEnabled();
                }
            };

}
