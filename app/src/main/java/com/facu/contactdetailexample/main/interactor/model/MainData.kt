package com.facu.contactdetailexample.main.interactor.model

import com.facu.contactdetailexample.main.Filter
import com.facu.contactdetailexample.main.interactor.behavior.sort.Sort

/**
 * This data will be displayed on Main screen.
 */
interface MainData {

    /**
     * The contact list.
     */
    val contactList: List<Contact>

    /**
     * The available filters.
     */
    val filterList: List<Filter>

    /**
     * The current filter.
     */
    val currentFilter: Filter?

    /**
     * The current sort.
     */
    val currentSort: Sort?
}