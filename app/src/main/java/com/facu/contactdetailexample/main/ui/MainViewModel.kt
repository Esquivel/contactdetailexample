package com.facu.contactdetailexample.main.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.facu.contactdetailexample.main.Filter
import com.facu.contactdetailexample.main.interactor.MainDataInteractor
import com.facu.contactdetailexample.main.interactor.model.Contact
import com.facu.contactdetailexample.main.SortType
import kotlinx.coroutines.launch

class MainViewModel(
    private val mainDataInteractor: MainDataInteractor
) : ViewModel() {

    fun getMainData() = mainDataInteractor.getMainData()

    fun onAddButtonClicked() {
        viewModelScope.launch {
            mainDataInteractor.onAddContactsCalled()
        }
    }

    fun onDeleteButtonClicked(contact: Contact) {
        viewModelScope.launch {
            mainDataInteractor.onDeleteContactCalled(contact)
        }
    }

    fun onFavContactCalled(contact: Contact) {
        viewModelScope.launch {
            mainDataInteractor.onFavContactCalled(contact)
        }
    }

    fun onFilterClicked(filter: Filter) {
        viewModelScope.launch {
            mainDataInteractor.onFilterContactCalled(filter)
        }
    }

    fun onSortClicked(sort: SortType) {
        viewModelScope.launch {
            mainDataInteractor.onSortChanged(sort)
        }
    }

    override fun onCleared() {
        super.onCleared()
        mainDataInteractor.stop()
    }
}