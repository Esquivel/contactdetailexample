package com.facu.contactdetailexample.main.interactor.repository

import com.facu.contactdetailexample.main.interactor.repository.model.Result
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ContactApiClient {

    @GET(".")
    suspend fun getContacts(@Query("results") results: Int): Response<Result>
}