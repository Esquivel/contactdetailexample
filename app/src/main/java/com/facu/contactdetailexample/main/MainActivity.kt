package com.facu.contactdetailexample.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.facu.contactdetailexample.R
import com.facu.contactdetailexample.detail.DetailActivity
import com.facu.contactdetailexample.main.interactor.model.Contact
import com.facu.contactdetailexample.main.ui.MainFragment

private const val CONTACT = "CONTACT"

class MainActivity : AppCompatActivity(), MainCallback {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, MainFragment())
                    .commitNow()
        }
    }

    override fun onClickDetail(contact: Contact) {
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra(CONTACT, contact)

        startActivity(intent)
    }
}