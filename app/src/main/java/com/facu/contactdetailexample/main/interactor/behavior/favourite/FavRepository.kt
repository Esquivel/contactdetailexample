package com.facu.contactdetailexample.main.interactor.behavior.favourite

import com.facu.contactdetailexample.main.interactor.model.Contact

/**
 * This repository is an abstraction of each action that user can apply to a contact and that
 * action is important so it must be saved and restored.
 *
 * Example of this action:
 *  - Fav
 */
interface FavRepository {

    /**
     * Load contacts.
     */
    fun load(): Map<String, Contact>

    /**
     * Save contacts.
     */
    fun save(contact: Contact)
}