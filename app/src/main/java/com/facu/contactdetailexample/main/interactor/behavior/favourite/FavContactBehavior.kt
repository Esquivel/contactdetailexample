package com.facu.contactdetailexample.main.interactor.behavior.favourite

import com.facu.contactdetailexample.main.interactor.behavior.Behavior
import com.facu.contactdetailexample.main.interactor.behavior.BehaviorType
import com.facu.contactdetailexample.main.interactor.behavior.InternalCallback
import com.facu.contactdetailexample.main.interactor.model.Contact
import com.facu.contactdetailexample.main.interactor.model.MainData
import com.facu.contactdetailexample.main.interactor.model.MainDataBuilder

class FavContactBehavior(
        private val favRepository: FavRepository,
        private val mainDataBuilder: MainDataBuilder
): Behavior<FavBehaviorTask> {

    override fun getType(): BehaviorType = BehaviorType.FAV

    override suspend fun process(
            previousData: MainData,
            specialData: FavBehaviorTask,
            callback: InternalCallback
    ) {
        val internalList: MutableList<Contact> = ArrayList(previousData.contactList)
        val contactMap = LinkedHashMap<String, Contact>()

        for (value in internalList) {
            contactMap[value.id] = value
        }

        specialData.contact?.run {
            favRepository.save(this)
        }

        for (value in favRepository.load()) {
            if (contactMap.containsKey(value.key)) {
                contactMap[value.key] = value.value
            }
        }

        callback.onFinishProcess(mainDataBuilder
                .withMainData(previousData)
                .withContactList(contactMap.values.toList())
                .build())
    }
}