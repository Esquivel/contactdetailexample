package com.facu.contactdetailexample.main.ui

import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.facu.contactdetailexample.R
import com.facu.contactdetailexample.main.MainProvider
import com.facu.contactdetailexample.commons.ViewModelFactory
import com.facu.contactdetailexample.main.MainCallback
import com.facu.contactdetailexample.main.interactor.model.Contact
import com.facu.contactdetailexample.main.SortType


class MainFragment : Fragment() {

    private var callback: MainCallback? = null

    private lateinit var viewModel: MainViewModel
    private lateinit var contactAdapter: ContactAdapter
    private lateinit var filterAdapter: FilterAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainCallback) {
            callback = context
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        contactAdapter = ContactAdapter()
        filterAdapter = FilterAdapter()

        viewModel = ViewModelProvider(
                this,
                ViewModelFactory(
                        this,
                        arguments,
                        MainProvider().provideInteractor()
                )
        )[MainViewModel::class.java]
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<View>(R.id.addButton).setOnClickListener {
            viewModel.onAddButtonClicked()
        }

        initSortRecyclerView(view)
        initContactRecyclerView(view)

        viewModel.getMainData().observe(viewLifecycleOwner, {
            contactAdapter.submitList(it.contactList)
            filterAdapter.submitList(it.filterList)

            view.findViewById<View>(R.id.welcomeMessage).visibility =
                if (it.contactList.isEmpty()) View.VISIBLE else View.GONE
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)

        addMenuBy(menu, SortType.LAST_NAME)
        addMenuBy(menu, SortType.FIRST_NAME)
        addMenuBy(menu, SortType.PHONE)
        addMenuBy(menu, SortType.COORDINATES)
        addMenuBy(menu, SortType.GENDER)
        addMenuBy(menu, SortType.MAIL)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        super.onOptionsItemSelected(item)
        viewModel.onSortClicked(getSortTypeBy(item))
        return true
    }

    override fun onResume() {
        super.onResume()
        filterAdapter.setCallback {
            viewModel.onFilterClicked(it)
        }

        contactAdapter.setCallback(object : ContactAdapter.Callback {
            override fun onContactClicked(contact: Contact) {
                callback?.onClickDetail(contact)
            }

            override fun onDeleteClicked(contact: Contact) {
                viewModel.onDeleteButtonClicked(contact)
            }

            override fun onFavClicked(contact: Contact) {
                viewModel.onFavContactCalled(contact)
            }
        })
    }

    override fun onPause() {
        super.onPause()
        filterAdapter.setCallback(null)
        contactAdapter.setCallback(null)
    }

    override fun onDetach() {
        super.onDetach()
        callback = null
    }

    private fun initSortRecyclerView(view: View) {
        val sortRecyclerView: RecyclerView = view.findViewById(R.id.sortRecyclerView)
        sortRecyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        sortRecyclerView.adapter = filterAdapter
    }

    private fun initContactRecyclerView(view: View) {
        val contactRecyclerView: RecyclerView = view.findViewById(R.id.contactRecyclerView)
        contactRecyclerView.layoutManager = LinearLayoutManager(context)
        contactRecyclerView.adapter = contactAdapter
    }

    private fun addMenuBy(menu: Menu, sortType: SortType) {
        menu.add(0, sortType.value, Menu.NONE, sortType.stringRes)
    }

    private fun getSortTypeBy(item: MenuItem): SortType {
        return when (item.itemId) {
            SortType.LAST_NAME.value -> SortType.LAST_NAME
            SortType.FIRST_NAME.value -> SortType.FIRST_NAME
            SortType.PHONE.value -> SortType.PHONE
            SortType.COORDINATES.value -> SortType.COORDINATES
            SortType.GENDER.value -> SortType.GENDER
            SortType.MAIL.value -> SortType.MAIL
            else -> {
                SortType.LAST_NAME
            }
        }
    }
}