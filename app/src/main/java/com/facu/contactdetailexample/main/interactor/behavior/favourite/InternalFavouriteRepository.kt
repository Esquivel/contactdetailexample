package com.facu.contactdetailexample.main.interactor.behavior.favourite

import com.facu.contactdetailexample.main.interactor.model.Contact

class InternalFavouriteRepository: FavRepository {

    private val map = HashMap<String, Contact>()

    override fun load(): Map<String, Contact> = map

    override fun save(contact: Contact) {
        if (!map.containsKey(contact.id)) {
            map[contact.id] = FavouriteContact(contact, true)
            return
        }

        map[contact.id]?.run {
            map[contact.id] = FavouriteContact(contact, !contact.favourite)
        }
    }
}