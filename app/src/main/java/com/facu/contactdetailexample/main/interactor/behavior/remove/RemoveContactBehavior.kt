package com.facu.contactdetailexample.main.interactor.behavior.remove

import com.facu.contactdetailexample.main.interactor.behavior.*
import com.facu.contactdetailexample.main.interactor.model.MainData
import com.facu.contactdetailexample.main.interactor.model.MainDataBuilder

class RemoveContactBehavior(
    private val mainDataBuilder: MainDataBuilder,
    private val ignoredRepository: ContactActionRepository
): Behavior<RemoveBehaviorTask> {

    override fun getType(): BehaviorType = BehaviorType.REMOVE

    override suspend fun process(
            previousData: MainData,
            specialData: RemoveBehaviorTask,
            callback: InternalCallback
    ) {
        val internalList = ArrayList(previousData.contactList)
        internalList.removeAll(specialData.inputData)
        ignoredRepository.save(specialData.inputData)

        callback.onFinishProcess(
            mainDataBuilder
                    .withMainData(previousData)
                    .withContactList(internalList)
                    .build()
        )
    }
}