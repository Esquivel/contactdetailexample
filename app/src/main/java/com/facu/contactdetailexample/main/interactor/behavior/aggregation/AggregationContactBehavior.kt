package com.facu.contactdetailexample.main.interactor.behavior.aggregation

import com.facu.contactdetailexample.main.interactor.ContactMapper
import com.facu.contactdetailexample.main.interactor.behavior.Behavior
import com.facu.contactdetailexample.main.interactor.behavior.BehaviorType
import com.facu.contactdetailexample.main.interactor.behavior.InternalCallback
import com.facu.contactdetailexample.main.interactor.behavior.ContactActionRepository
import com.facu.contactdetailexample.main.interactor.model.MainData
import com.facu.contactdetailexample.main.interactor.model.MainDataBuilder
import com.facu.contactdetailexample.main.interactor.repository.ContactRepository

class AggregationContactBehavior(
    private val contactRepository: ContactRepository,
    private val contactMapper: ContactMapper,
    private val mainDataBuilder: MainDataBuilder,
    private val ignoredRepository: ContactActionRepository
): Behavior<AggregationBehaviorTask> {

    override fun getType(): BehaviorType = BehaviorType.AGGREGATION

    override suspend fun process(
            previousData: MainData,
            specialData: AggregationBehaviorTask,
            callback: InternalCallback
    ) {
        val internalList = ArrayList(previousData.contactList)
        val contactList = contactRepository.getContacts()
        val targetContactList = contactMapper.map(contactList)

        internalList.addAll(targetContactList)
        internalList.removeAll(ignoredRepository.load())

        callback.onFinishProcess(
            mainDataBuilder
                    .withMainData(previousData)
                    .withContactList(internalList)
                    .build()
        )
    }
}