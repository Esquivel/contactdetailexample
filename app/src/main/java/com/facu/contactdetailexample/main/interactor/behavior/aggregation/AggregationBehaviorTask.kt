package com.facu.contactdetailexample.main.interactor.behavior.aggregation

import com.facu.contactdetailexample.main.interactor.behavior.BehaviorType
import com.facu.contactdetailexample.main.interactor.behavior.SimpleBehaviorTask

class AggregationBehaviorTask: SimpleBehaviorTask(BehaviorType.AGGREGATION)