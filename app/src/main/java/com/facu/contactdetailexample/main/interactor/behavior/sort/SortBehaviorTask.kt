package com.facu.contactdetailexample.main.interactor.behavior.sort

import com.facu.contactdetailexample.main.interactor.behavior.BehaviorType
import com.facu.contactdetailexample.main.interactor.behavior.BehaviorTask

class SortBehaviorTask(val currentSort: Sort? = null): BehaviorTask {

    override val type: BehaviorType
        get() = BehaviorType.SORT
}