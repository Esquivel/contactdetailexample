package com.facu.contactdetailexample.main.interactor.repository.model

import com.facu.contactdetailexample.main.interactor.model.Coordinates
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class LocationDto(
    val coordinates: CoordinatesDto?
): Serializable

data class CoordinatesDto(
    @SerializedName("latitude")
    val latDto: String?,
    @SerializedName("longitude")
    val lonDto: String?
): Coordinates {

    override val lat: String
        get() = latDto ?: ""

    override val lon: String
        get() = lonDto ?: ""
}