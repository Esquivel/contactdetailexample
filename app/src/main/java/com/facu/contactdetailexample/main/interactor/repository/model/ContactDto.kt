package com.facu.contactdetailexample.main.interactor.repository.model

import com.facu.contactdetailexample.main.interactor.model.Contact
import com.facu.contactdetailexample.main.interactor.model.Coordinates
import com.facu.contactdetailexample.main.interactor.model.Name
import com.facu.contactdetailexample.main.interactor.model.Picture
import com.google.gson.annotations.SerializedName
import java.io.Serializable

private const val EMPTY = ""

data class ContactDTO(
    val genderDto: String?,
    @SerializedName("id")
    val idDto: IdDto?,
    @SerializedName("location")
    val locationDto: LocationDto?,
    @SerializedName("email")
    val emailDto: String?,
    @SerializedName("phone")
    val phoneDto: String?,
    @SerializedName("name")
    val nameDto: NameDto?,
    @SerializedName("picture")
    val pictureDto: PictureDto?
): Contact {

    override val id: String
        get() = idDto?.value ?: EMPTY

    override val name: Name
        get() = nameDto ?: NameDto(EMPTY, EMPTY, EMPTY)

    override val gender: String
        get() = genderDto ?: EMPTY

    override val picture: Picture
        get() = pictureDto ?: PictureDto(EMPTY, EMPTY)

    override val coordinates: Coordinates
        get() = locationDto?.coordinates ?: CoordinatesDto(EMPTY, EMPTY)

    override val favourite: Boolean
        get() = false

    override val email: String
        get() = emailDto ?: EMPTY

    override val phone: String
        get() = phoneDto ?: EMPTY

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ContactDTO

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }
}