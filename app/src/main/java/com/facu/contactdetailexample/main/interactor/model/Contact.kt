package com.facu.contactdetailexample.main.interactor.model

import java.io.Serializable

/**
 * Contact data.
 */
interface Contact: Serializable {

    /**
     * Returns the id of this [Contact]
     */
    val id: String

    /**
     * Returns the name data.
     */
    val name: Name

    /**
     * Returns the gender.
     */
    val gender: String

    /**
     * Returns the picture data.
     */
    val picture: Picture

    /**
     * Returns the email data.
     */
    val email: String

    /**
     * Returns the phone data.
     */
    val phone: String

    /**
     * Return the coordinates.
     */
    val coordinates: Coordinates

    /**
     * Return true if this contact is favourite.
     */
    val favourite: Boolean
}