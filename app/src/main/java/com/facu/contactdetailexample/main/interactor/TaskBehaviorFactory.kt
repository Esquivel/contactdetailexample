package com.facu.contactdetailexample.main.interactor

import com.facu.contactdetailexample.main.Filter
import com.facu.contactdetailexample.main.interactor.behavior.aggregation.AggregationBehaviorTask
import com.facu.contactdetailexample.main.interactor.behavior.favourite.FavBehaviorTask
import com.facu.contactdetailexample.main.interactor.behavior.filter.FilterBehaviorTask
import com.facu.contactdetailexample.main.interactor.behavior.remove.RemoveBehaviorTask
import com.facu.contactdetailexample.main.interactor.behavior.sort.Sort
import com.facu.contactdetailexample.main.interactor.behavior.sort.SortBehaviorTask
import com.facu.contactdetailexample.main.interactor.model.Contact

class TaskBehaviorFactory {

    fun makeAggregation() = AggregationBehaviorTask()

    fun makeSort(currentSort: Sort? = null) = SortBehaviorTask(currentSort)

    fun makeRemove(contacts: List<Contact>) = RemoveBehaviorTask(contacts)

    fun makeFav(contact: Contact? = null) = FavBehaviorTask(contact)

    fun makeFilter(filter: Filter? = null) = FilterBehaviorTask(filter)
}