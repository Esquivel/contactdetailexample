package com.facu.contactdetailexample.main.interactor

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.facu.contactdetailexample.main.Filter
import com.facu.contactdetailexample.main.interactor.behavior.*
import com.facu.contactdetailexample.main.interactor.behavior.aggregation.AggregationBehaviorTask
import com.facu.contactdetailexample.main.interactor.behavior.favourite.FavBehaviorTask
import com.facu.contactdetailexample.main.interactor.behavior.filter.FilterBehaviorTask
import com.facu.contactdetailexample.main.interactor.behavior.remove.RemoveBehaviorTask
import com.facu.contactdetailexample.main.interactor.behavior.sort.SortBehaviorTask
import com.facu.contactdetailexample.main.interactor.model.Contact
import com.facu.contactdetailexample.main.interactor.model.MainData
import com.facu.contactdetailexample.main.interactor.behavior.sort.SortProvider
import com.facu.contactdetailexample.main.SortType
import kotlin.collections.ArrayList

class MainDataInteractor(
    private val behaviorMediator: BehaviorMediator,
    behaviorProvider: BehaviorProvider,
    private var mainData: MainData,
    private var sortProvider: SortProvider,
    private val taskBehaviorFactory: TaskBehaviorFactory
) {

    init {
        behaviorMediator.addBehavior(behaviorProvider.provideAggregationBehavior())
        behaviorMediator.addBehavior(behaviorProvider.provideSortBehavior())
        behaviorMediator.addBehavior(behaviorProvider.provideRemoveBehavior())
        behaviorMediator.addBehavior(behaviorProvider.provideFavBehavior())
        behaviorMediator.addBehavior(behaviorProvider.provideFilterBehavior())

        behaviorMediator.setCallback(object : ContactBehaviorCallback {
            override fun onFinish(data: MainData) {
                mainData = data

                mainDataLiveData.postValue(mainData)
            }
        })
    }

    private val mainDataLiveData = MutableLiveData<MainData>()

    /**
     * Returns the live data.
     */
    fun getMainData(): LiveData<MainData> = mainDataLiveData

    /**
     * Called when user add contacts.
     */
    suspend fun onAddContactsCalled() {
        val taskList = ArrayList<BehaviorTask>()
        taskList.add(taskBehaviorFactory.makeAggregation())
        taskList.add(taskBehaviorFactory.makeSort(mainData.currentSort))
        taskList.add(taskBehaviorFactory.makeFav())
        taskList.add(taskBehaviorFactory.makeFilter())

        behaviorMediator.start(mainData, taskList)
    }

    /**
     * Called when user change the sort.
     */
    suspend fun onSortChanged(newSort: SortType) {
        val taskList = ArrayList<BehaviorTask>()
        taskList.add(taskBehaviorFactory.makeSort(sortProvider.provideSortBy(newSort)))

        behaviorMediator.start(mainData, taskList)
    }

    /**
     * Called when user delete a contact.
     */
    suspend fun onDeleteContactCalled(contact: Contact) {
        val taskList = ArrayList<BehaviorTask>()
        taskList.add(taskBehaviorFactory.makeRemove(listOf(contact)))
        taskList.add(taskBehaviorFactory.makeSort(mainData.currentSort))

        behaviorMediator.start(mainData, taskList)
    }

    /**
     * Called when user add or remove a contact to favourite.
     */
    suspend fun onFavContactCalled(contact: Contact) {
        val taskList = ArrayList<BehaviorTask>()
        taskList.add(taskBehaviorFactory.makeFav(contact))
        taskList.add(taskBehaviorFactory.makeFilter())
        taskList.add(taskBehaviorFactory.makeSort(mainData.currentSort))

        behaviorMediator.start(mainData, taskList)
    }

    suspend fun onFilterContactCalled(filter: Filter) {
        val taskList = ArrayList<BehaviorTask>()
        taskList.add(taskBehaviorFactory.makeFilter(filter))
        taskList.add(taskBehaviorFactory.makeSort(mainData.currentSort))

        behaviorMediator.start(mainData, taskList)
    }

    /**
     * Call this method if you need to stop the current process.
     */
    fun stop() {
        behaviorMediator.stop()
    }
}