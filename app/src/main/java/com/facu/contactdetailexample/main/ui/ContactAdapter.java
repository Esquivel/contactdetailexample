package com.facu.contactdetailexample.main.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.facu.contactdetailexample.R;
import com.facu.contactdetailexample.main.interactor.model.Contact;
import com.facu.contactdetailexample.main.interactor.model.Name;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ContactAdapter extends ListAdapter<Contact, ContactAdapter.ItemViewHolder> {

    @Nullable
    private Callback callback;

    public ContactAdapter() {
        super(DIFF_CALLBACK);
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_contact, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        final Contact contact = getItem(position);

        final Context context = holder.image.getContext();
        Picasso.with(context)
                .load(contact.getPicture().getMiniUri())
                .fit()
                .centerCrop()
                .into(holder.image);

        holder.firstName.setText(contact.getName().getFirst());
        holder.lastName.setText(contact.getName().getLast());
        holder.mail.setText(context.getString(R.string.mail, contact.getEmail()));
        holder.phone.setText(context.getString(R.string.phone, contact.getPhone()));
        Picasso.with(context)
                .load(contact.getFavourite()
                        ? R.drawable.outline_favorite_black_48
                        : R.drawable.outline_favorite_border_black_48)
                .fit()
                .centerCrop()
                .into(holder.fav);

        holder.delete.setOnClickListener(view -> {
            if (callback != null) {
                callback.onDeleteClicked(contact);
            }
        });

        holder.fav.setOnClickListener(view -> {
            if (callback != null) {
                callback.onFavClicked(contact);
            }
        });

        holder.container.setOnClickListener(view -> {
            if (callback != null) {
                callback.onContactClicked(contact);
            }
        });
    }

    @Override
    public void submitList(@Nullable final List<Contact> list) {
        super.submitList(list != null ? new ArrayList<>(list) : null);
    }

    public void setCallback(@Nullable final Callback callback) {
        this.callback = callback;
    }

    static final class ItemViewHolder extends RecyclerView.ViewHolder {

        final View container;
        final ImageView image;
        final TextView firstName;
        final TextView lastName;
        final TextView mail;
        final TextView phone;
        final View delete;
        final ImageView fav;

        public ItemViewHolder(@NonNull final View itemView) {
            super(itemView);

            container = itemView.findViewById(R.id.container);
            image = itemView.findViewById(R.id.image);
            firstName = itemView.findViewById(R.id.firstName);
            lastName = itemView.findViewById(R.id.lastName);
            mail = itemView.findViewById(R.id.mail);
            phone = itemView.findViewById(R.id.phone);
            delete = itemView.findViewById(R.id.delete);
            fav = itemView.findViewById(R.id.fav);
        }
    }

    public interface Callback {

        void onContactClicked(@NonNull final Contact contact);

        void onDeleteClicked(@NonNull final Contact contact);

        void onFavClicked(@NonNull final Contact contact);
    }

    static final DiffUtil.ItemCallback<Contact> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<Contact>() {
                @Override
                public boolean areItemsTheSame(Contact oldItem, Contact newItem) {
                    return oldItem.getId().equals(newItem.getId());
                }
                @Override
                public boolean areContentsTheSame(Contact oldItem, Contact newItem) {
                    final Name oldName = oldItem.getName();
                    final Name newName = newItem.getName();

                    return (oldName.getFirst().equals(newName.getFirst())
                            && oldName.getLast().equals(newName.getLast())
                            && oldItem.getFavourite() == newItem.getFavourite());
                }
            };
}
