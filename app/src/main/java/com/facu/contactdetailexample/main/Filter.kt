package com.facu.contactdetailexample.main

import com.facu.contactdetailexample.main.interactor.behavior.filter.FilterType

interface Filter {

    /**
     * Returns the filter name.
     */
    val stringRes: Int

    /**
     * Returns the type.
     */
    val type: FilterType

    /**
     * Returns true if this filter is enabled.
     */
    val enabled: Boolean
}