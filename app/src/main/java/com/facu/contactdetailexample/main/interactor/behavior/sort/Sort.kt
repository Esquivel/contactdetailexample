package com.facu.contactdetailexample.main.interactor.behavior.sort

import com.facu.contactdetailexample.main.SortType

interface Sort {

    /**
     * Returns the filter name.
     */
    val stringRes: Int

    /**
     * Returns the type.
     */
    val type: SortType
}