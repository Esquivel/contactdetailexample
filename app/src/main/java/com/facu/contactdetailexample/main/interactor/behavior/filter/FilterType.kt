package com.facu.contactdetailexample.main.interactor.behavior.filter

import com.facu.contactdetailexample.R

enum class FilterType(val stringRes: Int) {

    NEAR_ME(R.string.near_me_filter_type_description),
    ONLY_FAV(R.string.only_fav_filter_type_description);
}