package com.facu.contactdetailexample.main.interactor.repository.model

import java.io.Serializable

data class IdDto(
    val name: String?,
    val value: String?
): Serializable