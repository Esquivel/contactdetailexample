package com.facu.contactdetailexample.main.interactor.behavior

import com.facu.contactdetailexample.main.interactor.model.MainData

interface Behavior<T: BehaviorTask> {

    /**
     * Return the type.
     */
    fun getType(): BehaviorType

    /**
     * Process the operation.
     *
     * @param previousData The data to process.
     */
    suspend fun process(
        previousData: MainData,
        specialData: T,
        callback: InternalCallback
    )
}