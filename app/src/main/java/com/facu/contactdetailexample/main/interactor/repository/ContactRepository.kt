package com.facu.contactdetailexample.main.interactor.repository

import androidx.annotation.WorkerThread
import com.facu.contactdetailexample.main.interactor.repository.model.ContactDTO

private const val TARGET_SIZE_LIST = 10

@WorkerThread
class ContactRepository(
    private val contactApi: ContactApiClient
) {

    /**
     * Obtains a list of contacts
     */
    suspend fun getContacts(): List<ContactDTO> {
        val result = contactApi.getContacts(TARGET_SIZE_LIST).body()

        if (result != null) {
            return result.contactList
        }

        return ArrayList()
    }
}