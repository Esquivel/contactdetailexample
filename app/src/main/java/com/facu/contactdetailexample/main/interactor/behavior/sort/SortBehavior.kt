package com.facu.contactdetailexample.main.interactor.behavior.sort

import com.facu.contactdetailexample.main.interactor.behavior.Behavior
import com.facu.contactdetailexample.main.interactor.behavior.BehaviorType
import com.facu.contactdetailexample.main.interactor.behavior.InternalCallback
import com.facu.contactdetailexample.main.interactor.model.Contact
import com.facu.contactdetailexample.main.interactor.model.MainData
import com.facu.contactdetailexample.main.interactor.model.MainDataBuilder
import com.facu.contactdetailexample.main.SortType

class SortBehavior(
        private val sortProvider: SortProvider,
        private val mainDataBuilder: MainDataBuilder
): Behavior<SortBehaviorTask> {

    override fun getType(): BehaviorType = BehaviorType.SORT

    override suspend fun process(
            previousData: MainData,
            specialData: SortBehaviorTask,
            callback: InternalCallback
    ) {
        val internalData: MutableList<Contact> = ArrayList(previousData.contactList)
        val currentSort = specialData.currentSort ?: sortProvider.provideDefaultSort()
        val sortedList = sort(currentSort, internalData)

        val aux = ArrayList(previousData.filterList)

        val resultMainData = mainDataBuilder
                .withFilterList(ArrayList(aux))
                .withSort(currentSort)
                .withContactList(sortedList)

                .build()

        callback.onFinishProcess(resultMainData)
    }

    private fun sort(targetSort: Sort, contacts: List<Contact>): List<Contact> {
        return contacts.sortedBy {
            when (targetSort.type) {
                SortType.FIRST_NAME -> it.name.first
                SortType.LAST_NAME -> it.name.last
                SortType.GENDER -> it.gender
                SortType.MAIL -> it.email
                SortType.PHONE -> it.phone
                else -> {
                    it.name.first
                }
            }
        }
    }
}