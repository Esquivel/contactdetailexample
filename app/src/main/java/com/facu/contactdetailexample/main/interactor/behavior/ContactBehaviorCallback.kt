package com.facu.contactdetailexample.main.interactor.behavior

import com.facu.contactdetailexample.main.interactor.model.MainData

interface ContactBehaviorCallback {

    /**
     * Returns the obtained data.
     */
    fun onFinish(data: MainData)
}