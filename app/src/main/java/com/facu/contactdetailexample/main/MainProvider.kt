package com.facu.contactdetailexample.main

import com.facu.contactdetailexample.main.interactor.MainDataInteractor
import com.facu.contactdetailexample.main.interactor.TaskBehaviorFactory
import com.facu.contactdetailexample.main.interactor.behavior.BehaviorMediator
import com.facu.contactdetailexample.main.interactor.behavior.BehaviorProvider
import com.facu.contactdetailexample.main.interactor.model.MainDataBuilder
import com.facu.contactdetailexample.main.interactor.behavior.sort.SortProvider

class MainProvider {

    fun provideInteractor(): MainDataInteractor {
        return MainDataInteractor(
            BehaviorMediator(),
            BehaviorProvider(),
            MainDataBuilder().build(),
            SortProvider(),
            TaskBehaviorFactory()
        )
    }
}