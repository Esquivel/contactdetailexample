package com.facu.contactdetailexample.commons.location

import android.location.Location

interface LocationFacadeCallback {

    /**
     * Called to notify an invalid operation call.
     *
     * @param message The message.
     */
    fun onInvalidOperation(message: String)

    /**
     * Called to notify an internal event. This could be used for log purposes.
     *
     * @param message The message.
     */
    fun onNotifyEvent(message: String)

    /**
     * Called when the location is updated with a new value.
     * This location always be a non-null value.
     *
     * @param location The location.
     */
    fun onLocationUpdated(location: Location)
}