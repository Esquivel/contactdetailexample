package com.facu.contactdetailexample.commons

import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import com.facu.contactdetailexample.main.interactor.MainDataInteractor
import com.facu.contactdetailexample.main.ui.MainViewModel

class ViewModelFactory(
    owner: SavedStateRegistryOwner,
    defaultArgs: Bundle? = null,
    private val mainDataInteractor: MainDataInteractor
) : AbstractSavedStateViewModelFactory(owner, defaultArgs) {

    override fun <T : ViewModel?> create(key: String, modelClass: Class<T>,
                                         state: SavedStateHandle
    ): T {
        return MainViewModel(mainDataInteractor) as T
    }
}
