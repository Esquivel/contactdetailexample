package com.facu.contactdetailexample.commons.location

import android.location.Location

/**
 * This Worker hides how we obtain a [Location] exposing an easy and limited interface.
 * Can return Location in with two different ways:
 * - Asynchronous by [LocationFacadeCallback.onLocationUpdated]
 * - Synchronous by [.getLastKnownLocation]
 *
 * In the Synchronous way sometimes you can receive a null [Location], this could be by a post reset
 * device or off-location option on Android device.
 * If you choose Asynchronous way you always receive a validated non-null [Location].
 * Some policies could be applied before you receive the [Location]
 */
interface LocationFacade {
    /**
     * Connect method, let operational this worker.
     *
     * Before call any method you must call to connect() at least one time.
     * Every time you call to [.disconnect], if you want to be operational you must call it again.
     *
     * No operation can be performed if the Worker is not connected.
     */
    fun connect()

    /**
     * Disconnect method. Stop internal operations.
     */
    fun disconnect()

    /**
     * Start a repeated operation to obtain the current location.
     * The `intervalMillis` value is really important because a too tiny value can drain the battery.
     *
     * @param intervalMillis The interval in millis.
     */
    fun startUpdates(intervalMillis: Long)

    /**
     * Stop the repeated update.
     */
    fun stopUpdates()

    /**
     * Return the last known [Location].
     * Sometimes can return a null value.
     *
     * @return The location or null.
     */
    val lastKnownLocation: Location?

    /**
     * Set the callback.
     *
     * @param callback The instance.
     */
    fun setCallback(callback: LocationFacadeCallback?)
}