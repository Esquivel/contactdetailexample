package com.facu.contactdetailexample.commons.location

import android.location.Location

/**
 * Dummy implementation, used like an implementation of LocationManager of GoogleApiClient.
 *
 * For interview purposes.
 */
class DummyLocationWorker: LocationFacade {

    private var callback: LocationFacadeCallback? = null

    override fun connect() {
        //Nothing to do.
    }

    override fun disconnect() {
        //Nothing to do.
    }

    override fun startUpdates(intervalMillis: Long) {
        callback?.onLocationUpdated(buildMadridLocation())
    }

    override fun stopUpdates() {
        //Nothing to do.
    }

    override val lastKnownLocation: Location?
        get() = null

    override fun setCallback(callback: LocationFacadeCallback?) {
        this.callback = callback
    }

    private fun buildMadridLocation(): Location {
        val location = Location("")
        location.latitude = 40.416775
        location.longitude = -3.703790
        return location
    }
}