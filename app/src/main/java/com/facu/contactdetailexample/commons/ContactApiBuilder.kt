package com.facu.contactdetailexample.commons

import com.facu.contactdetailexample.main.interactor.repository.ContactApiClient
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

private const val BASE_URL = "https://api.randomuser.me"

class ContactApiBuilder {

    private var timeout: Boolean = false
    private var timeoutValue = 0L
    private var interceptor: Boolean = false

    /**
     * Adds timeout to each request. By default has not any timeout.
     */
    fun withTimeout(timeoutInSeconds: Long): ContactApiBuilder {
        timeoutValue = timeoutInSeconds
        timeout = true
        return this
    }

    /**
     * Adds an interceptor to show the request and response on logs.
     */
    fun withLoggingInterceptor(): ContactApiBuilder {
        interceptor = true
        return this
    }

    /**
     * Build method, returns an instance of [ContactApiClient]
     */
    fun build(): ContactApiClient {
        val builder = createBuilder()
        val okHttpClientBuilder = OkHttpClient.Builder()

        addTimeoutIfNeeded(okHttpClientBuilder)
        addInterceptorIfNeeded(okHttpClientBuilder)

        return builder
            .client(okHttpClientBuilder.build())
            .build()
            .create(ContactApiClient::class.java)
    }

    private fun addTimeoutIfNeeded(httpClientBuilder: OkHttpClient.Builder) {
        if (timeout) {
            httpClientBuilder
                .readTimeout(timeoutValue, TimeUnit.SECONDS)
                .connectTimeout(timeoutValue, TimeUnit.SECONDS)
                .build()
        }
    }

    private fun addInterceptorIfNeeded(okHttpClientBuilder: OkHttpClient.Builder) {
        if (interceptor) {
            val logging = HttpLoggingInterceptor()
            logging.setLevel(HttpLoggingInterceptor.Level.BODY)


            okHttpClientBuilder.interceptors().add(Interceptor { chain: Interceptor.Chain ->
                val newRequest: Request = chain.request().newBuilder().build()
                chain.proceed(newRequest)
            })

            okHttpClientBuilder.addInterceptor(logging)
            okHttpClientBuilder.build()
        }
    }

    private fun createBuilder(): Retrofit.Builder {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
    }

    //    protected void onError(Throwable e) {

    //        if (e instanceof HttpException) {
    //            HttpException response = (HttpException) e;
    //            int code = response.code();
    //            if (code == HttpURLConnection.HTTP_NOT_FOUND) {
    //                this.onResponseNoData();
    //            } else if (code == HttpURLConnection.HTTP_CLIENT_TIMEOUT) {
    //                this.onResponseTimeOut();
    //            } else {
    //                this.onResponseError();
    //            }
    //        } else {
    //            this.onResponseConnectionError();
    //        }
    //    }
}