package com.facu.contactdetailexample.detail

import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.facu.contactdetailexample.R
import com.facu.contactdetailexample.main.interactor.model.Contact
import com.squareup.picasso.Picasso

private const val CONTACT = "CONTACT"

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.detail_activity)
        setToolbar()

        val contact = intent.extras?.getSerializable(CONTACT) as Contact

        contact?.run {

            Picasso.with(applicationContext)
                    .load(contact.picture.largeUri)
                    .fit()
                    .centerCrop()
                    .into(findViewById<ImageView>(R.id.image))

            supportActionBar?.title = this.name.last + " " + this.name.last
        }
    }

    private fun setToolbar() {
        val toolbar: Toolbar = findViewById(R.id.toolbarCollapsible)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationOnClickListener { onBackPressed() }
    }
}